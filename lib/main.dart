import 'package:flutter/material.dart';
import 'Dart:math';

void main() {

  runApp(BallPage());

}

class BallPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home:Scaffold(
        appBar: AppBar(
          title: Text('Ask me anything'),
          backgroundColor: Colors.blue[900],
        ),
        backgroundColor: Colors.blue,
        body: Ball()
      )
    );
  }
}

class Ball extends StatefulWidget {
  @override
  _BallState createState() => _BallState();
}

class _BallState extends State<Ball> {

  int prevBallNumber;
  int ballNumber = 1;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        FlatButton(
            child: Image.asset('images/ball$ballNumber.png'),
            onPressed: (){
              print(ballNumber);
              setState(() {
                prevBallNumber = ballNumber;
                ballNumber = Random().nextInt(5) + 1;
              });
            }
        ),
        SizedBox(
          height:20
        ),
        Text('You got ball number $ballNumber'),
        Text('your prev ball number $prevBallNumber')
      ],
    );
  }
}

